# Word masters 

>Link to the game - https://aquamarine-souffle-0d519e.netlify.app

This is game where the player is asked to guess the answer that is a five letter word. The rules are 
* The player has 6 guesses.
* Every entry word should be a valid english word.
* If the entered word contains the letters in that is also present in the answer word but the position of the letter is wrong then the corresponding block is indicated with a orange background color.
* If the entered word contains the letters in that is also present in the answer word and in the same position then the corresponding block is indicated with a green background color.

## part one - HTML Part
I made the HTML part using div tags and flex box. 

## part two - JavaScript part
* used eventlistener with keydown action and an async function.
* I used async function to make the code wait till the answer for the game is fetched from the api.
* The await keyword is used so that code does not move ahead without the answer or without validating the function.
  
## part three - CSS Part 
* The Css is done for easier and good gameplay for the user.
* we provide hints and instuctions using color variations.