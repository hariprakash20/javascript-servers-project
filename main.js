const letters = document.querySelectorAll('.letter');
const maxLength = 5;
let currentwordlength = 0;
let letter = '';
let word = "";
let answer ="";
let won = false;
let isValid;

const status = document.querySelector('.online-status');
if(navigator.onLine){
    status.innerHTML= "Online";    
}
else{
    status.innerHTML = "Internet connection not available"
}

getAnswer();

if(!won){
    document.addEventListener("keydown",async function (event) {
        key=event.key;
        if (!isLetter(event.key)) {
            if(key =='Enter' && word.length == maxLength){
                await validate(word);
                if(isValid){
                    colorcoding();
                    if(evaluate()){
                        won=true;
                    }
                    else{
                        currentwordlength++;
                        word="";
                    }
                    isValid=false;
                }
                else{   
                    for(i=0;i<maxLength;i++)
                    letters[currentwordlength*maxLength + i].classList.add('invalid');
                    setTimeout(function(){
                        for(i=0;i<maxLength;i++)
                        letters[currentwordlength*maxLength + i].classList.remove('invalid');
                    },500);
                }
            }
            else if(key == 'Backspace' && word != "")
            {
                letters[currentwordlength*maxLength + word.length-1].innerHTML = "";
                word = word.substring(0,word.length-1);
            }
            else
            event.preventDefault();
        }
        else if(currentwordlength<6)
        {
            letter = event.key;
            letter = letter.toUpperCase();
            addToGrid(letter);
        }
    });
}

async function getAnswer(){
    const promise = await fetch("https://words.dev-apis.com/word-of-the-day");
    const processedResponse = await promise.json();
    answer = processedResponse.word.toUpperCase();   
}

function isLetter(letter) {
    return /^[a-zA-Z]$/.test(letter);
  }

function addToGrid(letter){
    if(word.length < maxLength)
    {
        word += letter;
    }
    else
    {
        word[4] = letter;
    }
    letters[currentwordlength*maxLength + word.length-1].innerText = letter;

    if(word.length == maxLength)
    {
        validate(word);
    }
}

async function validate(word){
    const promise = await fetch('https://words.dev-apis.com/validate-word',{method:'POST', body: JSON.stringify({word: word})});
    const processedResponse = await promise.json();
    if(processedResponse.validWord)
    {
        isValid = true;
    }
    else{
        isValid = false;
    }
}

function evaluate()
{
    const result = document.querySelector('.result');
    if (word == answer){
        result.innerHTML ="Congratulations, You have won the game!";
        return true;
    }
    else {
        result.innerHTML = "Try Again";
        return false;
    }
}

function colorcoding(){
    let a = word.split("");
    let b = answer.split("");
    for(i=0;i<5;i++){
        if(a[i]==b[i])
        {
            letters[currentwordlength*maxLength + i].classList.add('correct-letter');
            b[i]='*';
        }
    }
    for(i=0;i<5;i++){
        for(j=0;j<5;j++){
            if(a[i]==b[j])
            {
                letters[currentwordlength*maxLength + i].classList.add('matching-letter');
                b[j]='*';
                break;
            }
            else
            {
                letters[currentwordlength*maxLength + i].classList.add('non-matching-letter');
            }
        }
    }
}


